
import sys
import os
import subprocess

# Sanity check arguments
# -------------------------------------------------------------------------------

if not len(sys.argv) > 2:
    print "Incorrect number of arguments!!"
    print "Usage: " + sys.argv[0] + " <mmf-dir> <targeted-cache-size-KB> [double-precision]"
    sys.exit("Example: python " + sys.argv[0] + "")
    

mtxDir = sys.argv[1]
targetedCacheSize = float(sys.argv[2])
dataSize = 4
if(len(sys.argv) > 3):
    if(sys.argv[3] == "double-precision"):
        dataSize = 8

# parse mmf-directory and mmf-name for each matrix
# -------------------------------------------------------------------------------

for mmfDir in os.listdir(mtxDir):
    
    notDir = False
    for c in mmfDir:
        if(c == "."):
            notDir = True
    
    if(notDir):
        continue
        
    lastIndexOfSlash = -1;
    for i in range (len(mmfDir), 0):
        if(mmfPath[i] == "/"):
            lastIndexOfSlash = i
            break

    mmfName = mmfDir
    mmfDir = mtxDir + "/" + mmfDir
    mmfPath = mmfDir + "/" + mmfName + ".mtx"
    
    # print mmfName
    # print mmfDir
    # print mmfPath
    
    # Calculate matrix size (CSR)
    # -------------------------------------------------------------------------------
    
    isSymmetric = False
    nnz = 0
    rowCount = 0
    colCount = 0
    for line in open(mmfPath):
        if(line[0] == '%'):
            if("symmetric" in line):
                isSymmetric = True
        else:
            elems = line.split(" ")
            rowCount = int(elems[0])
            colCount = int(elems[1])
            nnz = int(elems[2])
            break;
    
    if(isSymmetric):
        nnz = nnz * 2 - rowCount
    
    # print "nnz: ", nnz
    # print "rowCount: ", rowCount
    # print "colCount: ", colCount
    # print "isSymmetric: ", isSymmetric
    
    
    # Matrix size = 
    #    nnz * index-size(4) + 
    #    (rowCount + 1) * (index-size(4)) +
    #    nnz * data-size(dp:8, sp:4)
    
    matrixTotalSize = nnz * 4 + (rowCount + 1) * 4 + nnz * dataSize
    yVectorSize = rowCount * dataSize
    totalSizeKB = (matrixTotalSize + yVectorSize) / 1000.0
    
    # print "matrixTotalSize: ", matrixTotalSize
    # print "yVectorSize: ", yVectorSize
    # print "totalSizeKB: ", totalSizeKB
    
    # Calculate partition count (must be power of 2)
    # -------------------------------------------------------------------------------
    
    # real partition count
    partitionCountFP = totalSizeKB / targetedCacheSize
    
    # convert round real partition count to closest power of 2
    upperBound = 1
    while(upperBound < partitionCountFP):
        upperBound = upperBound * 2
    lowerBound = upperBound / 2
    
    partitionCount = upperBound
    if((upperBound - partitionCountFP) > (partitionCountFP - lowerBound)):
        partitionCount = lowerBound
        
    if(partitionCount == 0):
        partitionCount = 1
        
    # Call Seher's partitioning code
    # -------------------------------------------------------------------------------
    
    if(partitionCount > 1):
        # print "./rb", mmfPath + " -k " + str(partitionCount) + " -i 0.15 -p " + mmfDir + "/" + mmfName + "_" + str(int(targetedCacheSize)) + "KB.partVector"
        print mmfPath
        subprocess.call("./rb " + mmfPath + " -k " + str(partitionCount) + " -i 0.15 -p " + mmfDir + "/" + mmfName + "_" + str(int(targetedCacheSize)) + "KB.partVector", shell=True)
        fo = open(mmfDir + "/" + mmfName + "_" + str(int(targetedCacheSize)) + "KB.stats", "w")
        fo.write(str(partitionCount))
        fo.close()
        print "\n"


