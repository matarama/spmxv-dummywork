#!/usr/bin/python

import sys

if len(sys.argv) != 4:
    print "Usage: ." + sys.argv[0] + " <curr_path_1> <input_path_2> <next_path>"
    exit(0)
    
# read input files
input1 = []
input2 = []

f1 = open(sys.argv[1], 'r')
input1 = f1.readline().split(' ')

f2 = open(sys.argv[2], 'r')
for line in f2:
    if line != '' and line != ' ':
        input2.append(line[0:len(line)-1])


len1 = len(input1)
len2 = len(input2)
print "Matrix count in original file: ", len1
print "Matrix count in new file: ", len2
    
for mtx in input2:
    
    exist = False
    for curr in input1:
        if(mtx == curr):
            exist = True
    
    if exist == False:
        input1.append(mtx)

print "Matrix count in merged file: ", len(input1), " / ", (len1 + len2)

f3 = open(sys.argv[3], 'w')
for mtx in input1:
    f3.write(mtx + ' ')