from Tkinter import *
import scipy.sparse.coo
import scipy.io
import sys
from scipy.sparse.coo import coo_matrix
from __builtin__ import len
import matplotlib.pyplot as plt
from fileinput import close
from twisted.test.test_text import lineWidth

# quintet
# ----------------------------------------------

class quintet:
    """Similar to triplet in coo format except there are 2 additional fields to store ordering info"""
    
    def __init__(self, data, i, j, iOrd, jOrd):
        self.data = data
        self.i = i
        self.j = j
        self.iOrd = iOrd
        self.jOrd = jOrd
        
    def toString(self):
        return ("i=" + str(self.i) + " j=" + str(self.j) + " iOrd=" + str(self.iOrd) + " jOrd=" + str(self.jOrd) + " data=" + str(self.data))
        
    def compareToI(self, other):
        if(self.i < other.i):
            return -1
        elif(self.i < other.i):
            return 1
        else:
            if(self.j < other.j):
                return -1
            elif(self.j > other.j):
                return 1
            else:
                return 0

    def compareToJ(self, other):
        if(self.j < other.j):
            return -1
        elif(self.j < other.j):
            return 1
        else:
            if(self.i < other.i):
                return -1
            elif(self.i > other.i):
                return 1
            else:
                return 0
            
    def compareToIOrd(self, other):
        if(self.iOrd < other.iOrd):
            return -1
        elif(self.iOrd > other.iOrd):
            return 1
        else:
            if(self.jOrd < other.jOrd):
                return -1
            elif(self.jOrd > other.jOrd):
                return 1
            else:
                return 0

    def compareToJOrd(self, other):
        if(self.jOrd < other.jOrd):
            return -1
        elif(self.jOrd > other.jOrd):
            return 1
        else:
            if(self.iOrd < other.iOrd):
                return -1
            elif(self.iOrd > other.iOrd):
                return 1
            else:
                return 0

# Quintet Spm
# ----------------------------------------------

class Quintet_Spm:
    """ quintet spm format for sparse matrices """
    
    def __init__(self, rowCount = 0, colCount = 0, nnz = 0, quintets = [], dimR = [], dimC = []):
        self.rowCount = rowCount
        self.colCount = colCount
        self.nnz = nnz
        self.quintets = quintets
        self.dimR = dimR;
        self.dimC = dimC;
        
    def plot(self):
        """ Plot Quintet_Spm using OpenGL  """
        fig = plt.figure()
        ax = fig.add_subplot(111, axisbg='white')
        ax.plot(self.getJ(), self.getI(), 's', color='white', ms=1)
        
        ax.set_aspect('equal')
        for spine in ax.spines.values():
            spine.set_visible(False)
        ax.invert_yaxis()
        ax.set_aspect('equal')
        ax.set_xticks([])
        ax.set_yticks([])
        return ax
        
        
    def plotOrd(self):
        """ Plot Quintet_Spm ordered using OpenGL  """   
        fig = plt.figure()
        ax = fig.add_subplot(111, axisbg='white')
        ax.plot(self.getJOrd(), self.getIOrd(), 's', color='white', ms=1)
        
        stackR = []
        stackC = []
        currR = 0
        currC = 0
        for i in range(0, len(self.dimR)):
            r = self.dimR[i]
            c = self.dimC[i]    
            stackR.append(r)
            stackC.append(c)
            
            # Draw lines
            if(r >= 0):
                ax.plot([currC, currC + c], [currR, currR], color="green", linestyle='-', linewidth=1)
                ax.plot([currC + c, currC + c], [currR, currR + r], color="green", linestyle='-', linewidth=1)
                ax.plot([currC + c, currC], [currR + r, currR + r], color="green", linestyle='-', linewidth=1)
                ax.plot([currC, currC], [currR + r, currR], color="green", linestyle='-', linewidth=1)

                currR = currR + r
                currC = currC + c
            
            if(len(stackR) > 1):
              i = len(stackR) - 1
              print "i=", i, "i-1=", i - 1, "len=", len(stackR), stackR
              while(len(stackR) > 1 and (stackR[i] is not -1) and (stackR[i - 1] is not -1)):
                  r = stackR[i] + stackR[i - 1]
                  c = stackC[i - 2]
                  
                  ax.plot([currC, currC + c], [currR, currR], color="green", linestyle='-', linewidth=1)
                  ax.plot([currC + c, currC + c], [currR, currR - r], color="green", linestyle='-', linewidth=1)
                  ax.plot([currC + c, currC], [currR - r, currR - r], color="green", linestyle='-', linewidth=1)
                  ax.plot([currC, currC], [currR - r, currR], color="green", linestyle='-', linewidth=1)
                  
                  stackR.pop()
                  stackC.pop()
                  stackR.pop()
                  stackC.pop()
                  stackR.pop()
                  stackC.pop()
                  
                  stackR.append(r)
                  stackC.append(c)
                  i = i - 2
                  currC = currC + c
                  
                  print stackR
                  
        
        #ax.set_xlim(0, self.rowCount)
        #ax.set_ylim(0, self.colCount)
        ax.set_aspect('equal')
        for spine in ax.spines.values():
            spine.set_visible(False)
        ax.invert_yaxis()
        ax.set_aspect('equal')
        ax.set_xticks([])
        ax.set_yticks([])
        return ax 
        
    def quintetSpmToCooOrd(self):
        """ Convert to scipy.sparse.coo.coo_matrix using ordered row and column indices """
        data = []
        row = []
        col = []
        for i in range(0, self.nnz):
            data.append(self.quintets[i].data)
            row.append(self.quintets[i].iOrd)
            col.append(self.quintets[i].jOrd)
            
        return coo_matrix(data, (row, col))

    def getI(self):
        iList = []
        for i in range(0, self.nnz):
            iList.append(self.quintets[i].i)
        
        print "iList:", iList
        return iList
        
    def getJ(self):
        jList = []
        for i in range(0, self.nnz):
            jList.append(self.quintets[i].j)
            
        print "jList:", jList
        return jList
            
    def getIOrd(self):
        iOrdList = []
        for i in range(0, self.nnz):
            iOrdList.append(self.quintets[i].iOrd)
            
        print "iOrdList:", iOrdList
        return iOrdList
            
    def getJOrd(self):
        jOrdList = []
        for i in range(0, self.nnz):
            jOrdList.append(self.quintets[i].jOrd)
            
        print "jOrdList:", jOrdList
        return jOrdList

    def quintetSpmToCoo(self):
        """ Convert to scipy.sparse.coo.coo_matrix """
        data = []
        row = []
        col = []
        for i in range(0, self.nnz):
            data.append(self.quintets[i].data)
            row.append(self.quintets[i].i)
            col.append(self.quintets[i].j)
            
        return coo_matrix(data, (row, col))

    def _print(self):
        for i in range(0, self.nnz):
            print(str(i) + ".\t" + self.quintets[i].toString())

    def injectOrderingInfo(self, rowOrderLookup, colOrderLookup):
        if rowOrderLookup is not None:
            for i in range(0, len(rowOrderLookup)):
                self.quintets[i].iOrd = rowOrderLookup[self.quintets[i].i]
            print "not none"
                
        if colOrderLookup is not None:
            for i in range(0, len(colOrderLookup)):
                self.quintets[i].jOrd = colOrderLookup[self.quintets[i].j]
            print "not none"
    
    def injectDimInfo(self, partitionTreePath):
        file = open(partitionTreePath)
        for line in file:
            r, c = line.split()
            self.dimR.append(int(r))
            self.dimC.append(int(c))
        file.close()
            
    @staticmethod
    def quintetSpmFromCoo(coo):
        """ Create Quintet_Spm from scipy.sparse.coo.coo_matrix """
        rowCount = coo.row
        colCount = coo.col
        nnz = coo.nnz
        quintets = []
        
        for i in range(0, nnz):
            quintets.append(quintet(coo.data[i], coo.row[i], coo.col[i], coo.row[i], coo.col[i]))
        
        return Quintet_Spm(rowCount, colCount, nnz, quintets)


# Quicksort
# ----------------------------------------------

from random import randint

def quicksort(qs):
    """Sort quintets in their ordered row and column values"""
    quicksortOrd(qs.quintets, 0, qs.nnz - 1)

def quicksortOrd(qs, start, end):
    if((end - start) > 1):
        pivot = randomizedPartitionOrd(qs, start, end)
        
        for i in range(start, end):
            print str(i) + ".\t" + qs[i].toString()
        
        quicksortOrd(qs, start, pivot - 1)
        quicksortOrd(qs, pivot + 1, end)
    

def randomizedPartitionOrd(qs, start, end):
    randIndex = randint(start, end)
    print "pivot: ", qs[randIndex].toString()
    swap(qs, randIndex, end)
    pivot = qs[end]
    i = start - 1
    for j in range(start, end):
        if(qs[j].compareToIOrd(pivot) <= 0):
            i = i + 1
            swap(qs, i, j)
            
    swap(qs, i, end)
    return i
    

def swap(qs, i, j):
    temp = qs[i]
    qs[i] = qs[j]
    qs[j] = temp
    

def insertionSort(quintetSpm):
    for i in range(1, len(quintetSpm.quintets)):
        curr = quintetSpm.quintets[i]
        j = i - 1
        while(j >= 0 and curr.compareToIOrd(quintetSpm.quintets[j]) < 0):
            quintetSpm.quintets[j + 1] = quintetSpm.quintets[j];
            j = j - 1
        quintetSpm.quintets[j + 1] = curr
    

# Read matrix
# ----------------------------------------------

def readMMF(mmfPath):
    spmInfo = scipy.io.mminfo(mmfPath)
    spm = scipy.io.mmread(mmfPath)
    
    rowCount = int(spmInfo[0])
    colCount = int(spmInfo[1])
    nnz = int(spmInfo[2])
    
    print rowCount, colCount, nnz
    
    isSymmetric = False
    for i in range(3, len(spmInfo)):
        if(spmInfo[i] == "symmetric"):
            isSymmetric = True
            break
    
    print "isSymmetric:", isSymmetric
    
    newSpm = spm;
    if(isSymmetric):
        newRow = []
        newCol = []
        newData = []
        
        for i in range(0, int(nnz)):
            newRow.append(spm.row[i])
            newCol.append(spm.col[i])
            newData.append(spm.data[i])
            
            if(not spm.row[i] == spm.col[i]):
                newCol.append(spm.row[i])
                newRow.append(spm.col[i])
                newData.append(spm.data[i])
            
        #newSpm = coo_matrix((newData, (newRow, newCol)), shape=(rowCount, colCount))
    
    return newSpm #quintet_spm.quintetSpmFromCoo(newSpm);

# Read vector
# ----------------------------------------------
def readVector(vectorPath):
    v = []
    try:
        file = open(vectorPath)
        content = file.readlines()
        file.close()
        
        # skip vector length
        for i in range(1, len(content)):
            v.append(int(content[i]))
        
        
    except IOError as e:
        print "I/O error({0}): {1}".format(e.errno, e.strerror)
    
    return v


mmfPath = sys.argv[1]
cellOrderLookupPath = sys.argv[2]
netOrderLookupPath = sys.argv[3]
partitionTreePath = sys.argv[4]
coo = readMMF(mmfPath)
rowCount = len(coo.row)
colCount = len(coo.col)

qs = Quintet_Spm.quintetSpmFromCoo(coo)

cellOrderLookup = readVector(cellOrderLookupPath)
netOrderLookup = readVector(netOrderLookupPath)
qs.injectOrderingInfo(cellOrderLookup, netOrderLookup)
qs.injectDimInfo(partitionTreePath)

ax = qs.plot()
ax.figure.savefig(mmfPath + "_unordered.png")

insertionSort(qs)

ax2 = qs.plotOrd()
ax2.figure.savefig(mmfPath + "_ordered.png")



