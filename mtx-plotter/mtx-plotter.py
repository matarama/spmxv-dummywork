from Tkinter import *
import scipy.sparse.coo
import scipy.io
import sys
from scipy.sparse.coo import coo_matrix
import quintet
import Quintet_Spm
from Quintet_Spm import quintetSpmFromCoo
import Image, ImageDraw

# Read matrix
# ----------------------------------------------

def readMMF(mmfPath):
    spmInfo = scipy.io.mminfo(mmfPath)
    spm = scipy.io.mmread(mmfPath)
    
    rowCount = int(spmInfo[0])
    colCount = int(spmInfo[1])
    nnz = int(spmInfo[2])
    
    print rowCount, colCount, nnz
    
    isSymmetric = False
    for i in range(3, len(spmInfo)):
        if(spmInfo[i] == "symmetric"):
            isSymmetric = True
            break
    
    print "isSymmetric:", isSymmetric
    
    newSpm = spm;
    if(isSymmetric):
        newRow = []
        newCol = []
        newData = []
        
        for i in range(0, int(nnz)):
            newRow.append(spm.row[i])
            newCol.append(spm.col[i])
            newData.append(spm.data[i])
            
            if(not spm.row[i] == spm.col[i]):
                newCol.append(spm.row[i])
                newRow.append(spm.col[i])
                newData.append(spm.data[i])
            
        #newSpm = coo_matrix((newData, (newRow, newCol)), shape=(rowCount, colCount))
    
    return newSpm #quintet_spm.quintetSpmFromCoo(newSpm);

mmfPath = sys.argv[1]
coo = readMMF(mmfPath)
rowCount = len(coo.row)
colCount = len(coo.col)

canvas_width = 512
canvas_height = 512

qs = Quintet_Spm.quintetSpmFromCoo(coo)
print qs

biggerDim = rowCount
if(biggerDim < colCount):
    biggerDim = colCount

scale_col = float(canvas_width) / colCount;
scale_row = float(canvas_height) / rowCount;

if(canvas_width < colCount):
    scale_col = 1.0 / scale_col
if(canvas_height < rowCount):
    scale_row = 1.0 / scale_row

offset = 2
scale_row = 1
scale_col = 1

white = (255, 255, 255)
green = (0,0,0)
image1 = Image.new("RGB", (colCount / 4, rowCount / 4), white)
draw = ImageDraw.Draw(image1)

for i in range(0, coo.nnz):
    rowIndex = coo.row[i]
    colIndex = coo.col[i]
    draw.point([rowIndex * (scale_row) + offset, colIndex * (scale_col) + offset], green)

image1.save(sys.argv[1] +  ".jpg")


