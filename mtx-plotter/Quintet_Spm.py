
import quintet
from scipy.sparse.coo import coo_matrix

class Quintet_Spm:
    """ quintet spm format for sparse matrices """
    
    def __init__(self, rowCount = 0, colCount = 0, nnz = 0, quintets = []):
        self.rowCount = rowCount
        self.colCount = colCount
        self.nnz = nnz
        self.quintets = quintets
        
    def plot(self):
        """ Plot Quintet_Spm using OpenGL  """
        
    def plotOrd(self):
        """ Plot Quintet_Spm ordered using OpenGL  """    
        
    def quintetSpmToCooOrd(self):
        """ Convert to scipy.sparse.coo.coo_matrix using ordered row and column indices """
        data = []
        row = []
        col = []
        for i in range(0, self.nnz):
            data.append(self.quintets[i].data)
            row.append(self.quintets[i].iOrd)
            col.append(self.quintets[i].jOrd)
            
        return coo_matrix(data, (row, col))

    def quintetSpmToCoo(self):
        """ Convert to scipy.sparse.coo.coo_matrix """
        data = []
        row = []
        col = []
        for i in range(0, self.nnz):
            data.append(self.quintets[i].data)
            row.append(self.quintets[i].i)
            col.append(self.quintets[i].j)
            
        return coo_matrix(data, (row, col))

    @staticmethod
    def quintetSpmFromCoo(coo):
        """ Create Quintet_Spm from scipy.sparse.coo.coo_matrix """
        rowCount = coo.row
        colCount = coo.col
        nnz = coo.nnz
        quintets = []
        
        for i in range(0, nnz):
            quintets.append(quintet(coo.data[i], coo.row[i], coo.col[i], coo.row[i], coo.col[i]))
        
        return Quintet_Spm(rowCount, colCount, nnz, quintets)
