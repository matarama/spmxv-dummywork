
class quintet:
    """Similar to triplet in coo format except there are 2 additional fields to store ordering info"""
    
    def __init__(self, data, i, j, iOrd, jOrd):
        self.data = data
        self.i = i
        self.j = j
        self.iOrd = iOrd
        self.jOrd = jOrd
        
    def compareToI(self, other):
        if(self.i < other.i):
            return -1
        elif(self.i < other.i):
            return 1
        else:
            if(self.j < other.j):
                return -1
            elif(self.j > other.j):
                return 1
            else:
                return 0

    def compareToJ(self, other):
        if(self.j < other.j):
            return -1
        elif(self.j < other.j):
            return 1
        else:
            if(self.i < other.i):
                return -1
            elif(self.i > other.i):
                return 1
            else:
                return 0
            
    def compareToIOrd(self, other):
        if(self.iOrd < other.iOrd):
            return -1
        elif(self.iOrd < other.iOrd):
            return 1
        else:
            if(self.jOrd < other.jOrd):
                return -1
            elif(self.jOrd > other.jOrd):
                return 1
            else:
                return 0

    def compareToJOrd(self, other):
        if(self.jOrd < other.jOrd):
            return -1
        elif(self.jOrd < other.jOrd):
            return 1
        else:
            if(self.iOrd < other.iOrd):
                return -1
            elif(self.iOrd > other.iOrd):
                return 1
            else:
                return 0