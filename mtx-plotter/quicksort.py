
from random import randint

def quicksort(qs):
    """Sort quintets in their ordered row and column values"""
    quicksortOrd(qs.quintets, 0, qs.nnz)

def quicksortOrd(qs, start, end):
    if((end - start) > 1):
        pivot = randomizedPartitionOrd(qs, start, end)
        quicksortOrd(qs, start, pivot - 1)
        quicksortOrd(qs, pivot + 1, end)
    

def randomizedPartitionOrd(qs, start, end):
    randIndex = randint(start, end)
    swap(qs, randIndex, end)
    pivot = qs[end]
    i = start - 1
    for j in range(start, end):
        if(qs[j].compareToIOrd(pivot) <= 0):
            i = i + 1
            swap(qs, i, j)
            
    swap(qs, i, end)
    return i
    

def swap(qs, i, j):
    temp = qs[i]
    qs[i] = qs[j]
    qs[j] = temp
    